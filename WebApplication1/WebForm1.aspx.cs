using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        //You can get a valid token at https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes
        //Insert your JWT token INSERT_VALID_ACCESS_TOKEN
        static string INSERT_VALID_ACCESS_TOKEN = "TokenDeAutenticacao";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void VerificarCarimbo_Click(object sender, EventArgs e)
        {
            try
            {

                if (INSERT_VALID_ACCESS_TOKEN == "")
                    throw new Exception("⚠ Please Insert a valid Access Token At Line 22");

                //Chamada da função que manda a requisição para o serviço de verificação de carimbo
                string responseInitialize = VerificarCarimbo().Result;

                if (responseInitialize == null)
                throw new Exception(responseInitialize + "\\n⚠ Please, check if you have credits available and entered a valid JWT token.");

                // O resultado do BRy Framework é uma string com o JSON contendo o relatório de verificação
                this.TextBoxSaidaInicializar.Text = responseInitialize;

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alerts", "javascript:alert('" + ex.Message + "')", true);

            }

        }


        public async static Task<string> VerificarCarimbo()
        {
            var requestContent = new MultipartFormDataContent();

            //Buscar o caminho da aplicação
            string path = AppDomain.CurrentDomain.BaseDirectory;

            //Lendo o documento do disco e convertendo para Streamcontent
            var fileStream = new FileStream(path + "./documento.pdf", FileMode.Open);
            //Lendo o carimbo do disco e convertendo para Streamcontent
            var fileStreamCarimbo = new FileStream(path + "./carimboDoTempo-1.tst.pdf", FileMode.Open);

            var streamContentDocument = new StreamContent(fileStream);
            var streamContentCarimbo = new StreamContent(fileStreamCarimbo);

            // Calculando o HASH para envio na Requisição - Melhor performance em relação a enviar o documento inteiro
            byte[] hashBytes;

            using (fileStream)
            {
                System.Security.Cryptography.SHA256 sha256 = System.Security.Cryptography.SHA256.Create();
                hashBytes = sha256.ComputeHash(fileStream);
            }

            string hashString = BitConverter.ToString(hashBytes).Replace("-", String.Empty);

            requestContent.Add(new StringContent("123"), "nonce"); // Nonce da Requisição
            requestContent.Add(streamContentCarimbo, "timestamps[0][content]"); // O arquivo que contém o carimbo 
            requestContent.Add(new StringContent("1"), "timestamps[0][nonce]"); // Nonce do documento
            requestContent.Add(new StringContent(hashString), "timestamps[0][documentHash]"); // Documento original - Neste exemplo é o HASH de um documento PDF
            requestContent.Add(new StringContent("false"), "contentReturn"); // Relatório com a chave pública do certificado da cadeia e carimbo codificada na base64
            requestContent.Add(new StringContent("CHAIN"), "mode");

            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.Add("Authorization", INSERT_VALID_ACCESS_TOKEN);

            //Envia os dados para emissão do Carimbo do Tempo em formato Multipart
            var response = await client.PostAsync("https://fw2.bry.com.br/api/carimbo-service/v1/timestamps/verify", requestContent).ConfigureAwait(false);

            // O resultado do BRy Framework é uma string com o JSON contendo os carimbos
            string resposta = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            return resposta;
        }

        public static string Serialize<T>(T obj)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            MemoryStream ms = new MemoryStream();
            serializer.WriteObject(ms, obj);
            string retVal = Encoding.UTF8.GetString(ms.ToArray());
            return retVal;
        }

        public static T Deserialize<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            obj = (T)serializer.ReadObject(ms);
            ms.Close();
            return obj;
        }

    }

}
